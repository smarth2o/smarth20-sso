/**
 * 
 */
package ro.setmobile.idprovider.saml.beans;

import org.opensaml.saml2.core.EncryptedAssertion;

/**
 * @author dan
 * 
 */
public class IDPAssertion {

	private String			assertion;
	private EncryptedAssertion	encryptedAssertion;

	/**
	 * 
	 * @param assertion
	 */
	public IDPAssertion(String assertion) {

		this.assertion = assertion;
	}

	/**
	 * @return the assertion
	 */
	public String getAssertion() {

		return assertion;
	}

	/**
	 * @param assertion
	 *            the assertion to set
	 */
	public void setAssertion(String assertion) {

		this.assertion = assertion;
	}

	/**
	 * @return the encryptedAssertion
	 */
	public EncryptedAssertion getEncryptedAssertion() {

		return encryptedAssertion;
	}

	/**
	 * @param encryptedAssertion
	 *            the encryptedAssertion to set
	 */
	public void setEncryptedAssertion(EncryptedAssertion encryptedAssertion) {

		this.encryptedAssertion = encryptedAssertion;
	}

}
