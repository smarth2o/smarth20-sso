/**
 * 
 */
package ro.setmobile.idprovider.saml.beans;

import org.springframework.security.core.GrantedAuthority;

/**
 * @author dan
 * 
 */
@SuppressWarnings("serial")
public class Authority implements GrantedAuthority {

	private String	authority;

	/**
	 * 
	 * @param authority
	 */
	public Authority(String authority) {

		this.authority = authority;
	}

	/**
	 * @return the authority
	 */
	public String getAuthority() {

		return authority;
	}

	/**
	 * @param authority
	 *            the authority to set
	 */
	public void setAuthority(String authority) {

		this.authority = authority;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.lang.Object#hashCode()
	 */
	@Override
	public int hashCode() {

		final int prime = 31;
		int result = 1;
		result = (prime * result) + ((authority == null) ? 0 : authority.hashCode());
		return result;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.lang.Object#equals(java.lang.Object)
	 */
	@Override
	public boolean equals(Object obj) {

		if (this == obj) {
			return true;
		}
		if (obj == null) {
			return false;
		}
		if (getClass() != obj.getClass()) {
			return false;
		}
		Authority other = (Authority) obj;
		if (authority == null) {
			if (other.authority != null) {
				return false;
			}
		}
		else if (!authority.equals(other.authority)) {
			return false;
		}
		return true;
	}

}
