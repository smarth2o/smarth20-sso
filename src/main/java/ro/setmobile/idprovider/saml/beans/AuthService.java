/**
 * 
 */
package ro.setmobile.idprovider.saml.beans;

/**
 * @author dan
 * 
 */
public class AuthService {

	private String	destinationUrl;

	/**
	 * 
	 * @param service
	 */
	public AuthService() {

	}

	/**
	 * @return the destinationUrl
	 */
	public String getDestinationUrl() {
		return destinationUrl;
	}

	/**
	 * @param destinationUrl
	 *            the destinationUrl to set
	 */
	public void setDestinationUrl(String destinationUrl) {
		this.destinationUrl = destinationUrl;
	}

}
