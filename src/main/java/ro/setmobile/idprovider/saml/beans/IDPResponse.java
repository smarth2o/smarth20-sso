/**
 * 
 */
package ro.setmobile.idprovider.saml.beans;

/**
 * @author dan
 * 
 */
public class IDPResponse {

	public String	respone;

	/**
	 * 
	 * @param response
	 */
	public IDPResponse(String response) {

		respone = response;
	}

	/**
	 * @return the respone
	 */
	public String getRespone() {

		return respone;
	}

	/**
	 * @param respone
	 *            the respone to set
	 */
	public void setRespone(String respone) {

		this.respone = respone;
	}

}
