package ro.setmobile.idprovider.saml.manager;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.StringWriter;
import java.io.UnsupportedEncodingException;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import java.util.Set;

import javax.xml.namespace.QName;
import javax.xml.transform.TransformerException;

import org.apache.log4j.Logger;
import org.jdom.Document;
import org.joda.time.DateTime;
import org.joda.time.DateTimeZone;
import org.opensaml.Configuration;
import org.opensaml.DefaultBootstrap;
import org.opensaml.common.SAMLObjectBuilder;
import org.opensaml.common.SAMLVersion;
import org.opensaml.saml2.core.Assertion;
import org.opensaml.saml2.core.Attribute;
import org.opensaml.saml2.core.AttributeStatement;
import org.opensaml.saml2.core.AttributeValue;
import org.opensaml.saml2.core.Audience;
import org.opensaml.saml2.core.AudienceRestriction;
import org.opensaml.saml2.core.AuthnContext;
import org.opensaml.saml2.core.AuthnContextClassRef;
import org.opensaml.saml2.core.AuthnStatement;
import org.opensaml.saml2.core.Conditions;
import org.opensaml.saml2.core.Issuer;
import org.opensaml.saml2.core.LogoutResponse;
import org.opensaml.saml2.core.NameID;
import org.opensaml.saml2.core.NameIDType;
import org.opensaml.saml2.core.Response;
import org.opensaml.saml2.core.Status;
import org.opensaml.saml2.core.StatusCode;
import org.opensaml.saml2.core.StatusMessage;
import org.opensaml.saml2.core.Subject;
import org.opensaml.saml2.core.SubjectConfirmation;
import org.opensaml.saml2.core.SubjectConfirmationData;
import org.opensaml.saml2.core.impl.AssertionMarshaller;
import org.opensaml.xml.ConfigurationException;
import org.opensaml.xml.XMLObject;
import org.opensaml.xml.XMLObjectBuilder;
import org.opensaml.xml.XMLObjectBuilderFactory;
import org.opensaml.xml.io.Marshaller;
import org.opensaml.xml.io.MarshallingException;
import org.opensaml.xml.io.Unmarshaller;
import org.opensaml.xml.io.UnmarshallingException;
import org.opensaml.xml.parse.ParserPool;
import org.opensaml.xml.parse.XMLParserException;
import org.opensaml.xml.schema.XSString;
import org.opensaml.xml.security.x509.BasicX509Credential;
import org.opensaml.xml.security.x509.X509Credential;
import org.opensaml.xml.signature.KeyInfo;
import org.opensaml.xml.signature.Signature;
import org.opensaml.xml.signature.SignatureConstants;
import org.opensaml.xml.signature.impl.KeyInfoBuilder;
import org.opensaml.xml.signature.impl.SignatureBuilder;
import org.opensaml.xml.util.XMLHelper;
import org.springframework.stereotype.Component;
import org.w3c.dom.Element;

import ro.setmobile.idprovider.saml.beans.AuthService;
import ro.setmobile.idprovider.saml.beans.AuthUser;
import ro.setmobile.idprovider.saml.utilities.Constants;
import ro.setmobile.idprovider.saml.utilities.IDGenerator;
import ro.setmobile.idprovider.saml.utilities.SAMLException;
import ro.setmobile.idprovider.saml.utilities.SAMLUtil;

/**
 * SAML 2.0 Assertion.
 */
@Component("samlManager")
public class SAMLManager {

	protected static Logger					logger	= Logger.getLogger(SAMLManager.class.getName());

	private static XMLObjectBuilderFactory	builderFactory;

	/**
	 * 
	 */
	@SuppressWarnings({ "rawtypes", "unchecked" })
	public String doAssertion(AuthUser user, AuthService service, String nameID) {
		long startTime = new Date().getTime();

		String result = "";
		try {

			SAMLInputContainer input = new SAMLInputContainer();
			input.setStrIssuer(Constants.ISSUER);
			input.setStrNameQualifier(Constants.WEB_SITE);
			input.setStrNameID(nameID);

			// TODO session timeout
			input.setMaxSessionTimeoutInMinutes(Constants.SESSION_EXPIRE);
			input.setSessionId(IDGenerator.randomID());
			Map customAttributes = new HashMap();

			if (user != null) {
				customAttributes.put(Constants.NICKNAME, user.getUsername());
				customAttributes.put(Constants.EXPUID, user.getEmail());
			}
			else {
				// The user is unknown
				customAttributes.put(Constants.NICKNAME, null);
				customAttributes.put(Constants.EXPUID, null);
			}
			customAttributes.put(Constants.DESTINATION, service.getDestinationUrl());
			input.setAttributes(customAttributes);

			Assertion assertion = SAMLManager.buildDefaultAssertion(input);
			// set assertion ID
			assertion.setID(nameID);
			AssertionMarshaller marshaller = new AssertionMarshaller();
			Element plaintextElement = marshaller.marshall(assertion);
			result = XMLHelper.nodeToString(plaintextElement);

			logger.debug("Assertion String: " + result);

		}
		catch (MarshallingException e) {
			logger.error(e.getLocalizedMessage(), e);
		}
		finally {
			if (logger.isDebugEnabled()) {
				logger.debug("doAssertion: end - " + (new Date().getTime() - startTime) + " ms");
			}
		}

		return result;
	}

	/**
	 * 
	 * @param authnRequest
	 * @return
	 */
	public String getUserNameFromRequest(String authnRequest) {

		String result = "";

		try {
			result = this.getAuthProperty(authnRequest, Constants.USER_NAME);
		}
		catch (SAMLException e) {
			logger.error(e.getLocalizedMessage(), e);
		}

		return result;
	}

	/**
	 * 
	 * @param authnRequest
	 * @return
	 */
	public String getNameIDFromAuthnRequest(String authnRequest) {

		String result = "";

		try {
			result = this.getAuthProperty(authnRequest, Constants.NAME_ID);
		}
		catch (SAMLException e) {
			logger.error(e.getLocalizedMessage(), e);
		}

		return result;
	}

	/**
	 * 
	 * @param assertion
	 * @return
	 */
	public String getServiceCodeFromAssertion(String authnRequest) {

		String result = "";

		try {
			result = this.getAuthProperty(authnRequest, Constants.SERVICE_URL_NAME);
		}
		catch (SAMLException e) {
			logger.error(e.getLocalizedMessage(), e);
		}

		return result;

	}

	/**
	 * 
	 * @return
	 * @throws ConfigurationException
	 */
	public static XMLObjectBuilderFactory getSAMLBuilder() throws ConfigurationException {

		if (builderFactory == null) {
			DefaultBootstrap.bootstrap();
			builderFactory = org.opensaml.xml.Configuration.getBuilderFactory();
		}

		return builderFactory;
	}

	/**
	 * Builds a SAML Attribute of type String
	 * 
	 * @param name
	 * @param value
	 * @param builderFactory
	 * @return
	 * @throws ConfigurationException
	 */
	@SuppressWarnings("rawtypes")
	public static Attribute buildStringAttribute(String name, String value, XMLObjectBuilderFactory builderFactory) throws ConfigurationException {
		long startTime = new Date().getTime();

		if (logger.isDebugEnabled()) {
			logger.debug("buildStringAttribute: starting");
		}

		SAMLObjectBuilder attrBuilder = (SAMLObjectBuilder) getSAMLBuilder().getBuilder(Attribute.DEFAULT_ELEMENT_NAME);

		Attribute attrFirstName = (Attribute) attrBuilder.buildObject();

		attrFirstName.setName(name);

		// Set custom Attributes
		XMLObjectBuilder stringBuilder = getSAMLBuilder().getBuilder(XSString.TYPE_NAME);
		XSString attrValueFirstName = (XSString) stringBuilder.buildObject(AttributeValue.DEFAULT_ELEMENT_NAME, XSString.TYPE_NAME);
		attrValueFirstName.setValue(value);

		attrFirstName.getAttributeValues().add(attrValueFirstName);

		if (logger.isDebugEnabled()) {
			logger.debug("buildStringAttribute: end - " + (new Date().getTime() - startTime) + " ms");
		}

		return attrFirstName;
	}

	/**
	 * 
	 * @param input
	 * @return
	 */
	@SuppressWarnings({ "rawtypes", "unchecked" })
	public static Assertion buildDefaultAssertion(SAMLInputContainer input) {
		Assertion assertion = null;
		try {

			// Create the NameIdentifier
			SAMLObjectBuilder nameIdBuilder = (SAMLObjectBuilder) SAMLManager.getSAMLBuilder().getBuilder(NameID.DEFAULT_ELEMENT_NAME);
			NameID nameId = (NameID) nameIdBuilder.buildObject();
			nameId.setValue(input.getStrNameID());
			nameId.setNameQualifier(input.getStrNameQualifier());
			nameId.setFormat(NameIDType.UNSPECIFIED);

			// Create the SubjectConfirmation
			SAMLObjectBuilder confirmationMethodBuilder = (SAMLObjectBuilder) SAMLManager.getSAMLBuilder().getBuilder(SubjectConfirmationData.DEFAULT_ELEMENT_NAME);
			SubjectConfirmationData confirmationMethod = (SubjectConfirmationData) confirmationMethodBuilder.buildObject();
			DateTime now = new DateTime(DateTimeZone.UTC);
			confirmationMethod.setNotBefore(now);

			SAMLObjectBuilder subjectConfirmationBuilder = (SAMLObjectBuilder) SAMLManager.getSAMLBuilder().getBuilder(SubjectConfirmation.DEFAULT_ELEMENT_NAME);
			SubjectConfirmation subjectConfirmation = (SubjectConfirmation) subjectConfirmationBuilder.buildObject();
			subjectConfirmation.setSubjectConfirmationData(confirmationMethod);

			// Create the Subject
			SAMLObjectBuilder subjectBuilder = (SAMLObjectBuilder) SAMLManager.getSAMLBuilder().getBuilder(Subject.DEFAULT_ELEMENT_NAME);
			Subject subject = (Subject) subjectBuilder.buildObject();

			subject.setNameID(nameId);
			subject.getSubjectConfirmations().add(subjectConfirmation);

			// Create Authentication Statement
			SAMLObjectBuilder authStatementBuilder = (SAMLObjectBuilder) SAMLManager.getSAMLBuilder().getBuilder(AuthnStatement.DEFAULT_ELEMENT_NAME);
			AuthnStatement authnStatement = (AuthnStatement) authStatementBuilder.buildObject();
			DateTime now2 = new DateTime(DateTimeZone.UTC);
			authnStatement.setAuthnInstant(now2);
			authnStatement.setSessionIndex(input.getSessionId());
			authnStatement.setSessionNotOnOrAfter(now2.plus(input.getMaxSessionTimeoutInMinutes()));

			SAMLObjectBuilder authContextBuilder = (SAMLObjectBuilder) SAMLManager.getSAMLBuilder().getBuilder(AuthnContext.DEFAULT_ELEMENT_NAME);
			AuthnContext authnContext = (AuthnContext) authContextBuilder.buildObject();

			SAMLObjectBuilder authContextClassRefBuilder = (SAMLObjectBuilder) SAMLManager.getSAMLBuilder().getBuilder(AuthnContextClassRef.DEFAULT_ELEMENT_NAME);
			AuthnContextClassRef authnContextClassRef = (AuthnContextClassRef) authContextClassRefBuilder.buildObject();
			authnContextClassRef.setAuthnContextClassRef(Constants.CONTEXTCLASS_REF);

			authnContext.setAuthnContextClassRef(authnContextClassRef);
			authnStatement.setAuthnContext(authnContext);

			// Builder Attributes
			SAMLObjectBuilder attrStatementBuilder = (SAMLObjectBuilder) SAMLManager.getSAMLBuilder().getBuilder(AttributeStatement.DEFAULT_ELEMENT_NAME);
			AttributeStatement attrStatement = (AttributeStatement) attrStatementBuilder.buildObject();

			// Create the attribute statement
			Map attributes = input.getAttributes();
			if (attributes != null) {
				Set<String> keySet = attributes.keySet();
				for (String key : keySet) {
					Attribute attrFirstName = buildStringAttribute(key, (String) attributes.get(key), getSAMLBuilder());
					attrStatement.getAttributes().add(attrFirstName);
				}
			}

			SAMLObjectBuilder audienceRestrictionBuilder = (SAMLObjectBuilder) SAMLManager.getSAMLBuilder().getBuilder(AudienceRestriction.DEFAULT_ELEMENT_NAME);
			AudienceRestriction audienceRestriction = (AudienceRestriction) audienceRestrictionBuilder.buildObject();

			SAMLObjectBuilder audienceBuilder = (SAMLObjectBuilder) SAMLManager.getSAMLBuilder().getBuilder(Audience.DEFAULT_ELEMENT_NAME);
			Audience audience = (Audience) audienceBuilder.buildObject();
			audience.setAudienceURI(input.getStrIssuer());

			audienceRestriction.getAudiences().add(audience);

			SAMLObjectBuilder conditionsBuilder = (SAMLObjectBuilder) SAMLManager.getSAMLBuilder().getBuilder(Conditions.DEFAULT_ELEMENT_NAME);
			Conditions conditions = (Conditions) conditionsBuilder.buildObject();

			conditions.getAudienceRestrictions().add(audienceRestriction);
			conditions.setNotBefore(now);

			// Create Issuer
			SAMLObjectBuilder issuerBuilder = (SAMLObjectBuilder) SAMLManager.getSAMLBuilder().getBuilder(Issuer.DEFAULT_ELEMENT_NAME);
			Issuer issuer = (Issuer) issuerBuilder.buildObject();
			issuer.setValue(input.getStrIssuer());

			// Create the assertion
			SAMLObjectBuilder assertionBuilder = (SAMLObjectBuilder) SAMLManager.getSAMLBuilder().getBuilder(Assertion.DEFAULT_ELEMENT_NAME);
			assertion = (Assertion) assertionBuilder.buildObject();
			assertion.setIssuer(issuer);
			assertion.setIssueInstant(now);
			assertion.setVersion(SAMLVersion.VERSION_20);

			assertion.getAuthnStatements().add(authnStatement);
			assertion.getAttributeStatements().add(attrStatement);
			assertion.setConditions(conditions);
			assertion.setSubject(subject);

		}
		catch (Exception e) {
			logger.error(e.getLocalizedMessage(), e);
		}
		finally {
		}

		return assertion;
	}

	/**
	 * 
	 * @param assertionStr
	 * @param inResponseTo
	 * @return
	 * @throws IOException
	 * @throws MarshallingException
	 * @throws TransformerException
	 * @throws ConfigurationException
	 * @throws SAMLException
	 */
	@SuppressWarnings("rawtypes")
	public Response createResponse(String assertionStr, String inResponseTo) throws IOException, MarshallingException, TransformerException, ConfigurationException, SAMLException {
		Response response = null;
		try {
			response = createResponse(StatusCode.SUCCESS_URI, null, inResponseTo);

			SAMLObjectBuilder assertionBuilder = (SAMLObjectBuilder) SAMLManager.getSAMLBuilder().getBuilder(Assertion.DEFAULT_ELEMENT_NAME);
			Assertion assertion = (Assertion) assertionBuilder.buildObject();
			assertion.setDOM(SAMLUtil.toDom(SAMLUtil.createJdomDoc(assertionStr).getRootElement()));
			response.getAssertions().add(assertion);

		}
		catch (Exception e) {
			logger.error(e.getMessage(), e);
		}
		finally {
		}

		return response;
	}

	/**
	 * 
	 * @param statusCode
	 * @param message
	 * @param inResponseTo
	 * @return
	 * @throws IOException
	 * @throws MarshallingException
	 * @throws TransformerException
	 * @throws ConfigurationException
	 */
	@SuppressWarnings("rawtypes")
	private Response createResponse(String statusCode, String message, String inResponseTo) throws IOException, MarshallingException, TransformerException, ConfigurationException {
		Response response = null;
		try {

			response = (Response) ((SAMLObjectBuilder) SAMLManager.getSAMLBuilder().getBuilder(Response.DEFAULT_ELEMENT_NAME)).buildObject();
			response.setID(IDGenerator.randomID());

			if (inResponseTo != null) {
				response.setInResponseTo(inResponseTo);
			}

			DateTime now = new DateTime(DateTimeZone.UTC);
			response.setIssueInstant(now);

			SAMLObjectBuilder statusCodeBuilder = (SAMLObjectBuilder) SAMLManager.getSAMLBuilder().getBuilder(StatusCode.DEFAULT_ELEMENT_NAME);
			StatusCode statusCodeElement = (StatusCode) statusCodeBuilder.buildObject();
			statusCodeElement.setValue(statusCode);

			SAMLObjectBuilder statusBuilder = (SAMLObjectBuilder) SAMLManager.getSAMLBuilder().getBuilder(Status.DEFAULT_ELEMENT_NAME);
			Status status = (Status) statusBuilder.buildObject();
			status.setStatusCode(statusCodeElement);
			response.setStatus(status);

			if (message != null) {

				SAMLObjectBuilder statusMessageBuilder = (SAMLObjectBuilder) SAMLManager.getSAMLBuilder().getBuilder(StatusMessage.DEFAULT_ELEMENT_NAME);
				StatusMessage statusMessage = (StatusMessage) statusMessageBuilder.buildObject();
				statusMessage.setMessage(message);
				status.setStatusMessage(statusMessage);
			}
		}
		catch (Exception e) {
			logger.error(e.getMessage(), e);
		}
		finally {
		}

		return response;
	}

	/**
	 * 
	 * @param statusCode
	 * @param message
	 * @param inResponseTo
	 * @return
	 * @throws IOException
	 * @throws MarshallingException
	 * @throws TransformerException
	 * @throws ConfigurationException
	 */
	@SuppressWarnings({ "rawtypes", "unchecked" })
	public LogoutResponse createLogoutResponse(String statusCode, String inResponseTo, String issuerURL, String destinationURL, String nameID) throws IOException, MarshallingException, TransformerException, ConfigurationException {
		LogoutResponse response = null;

		try {

			response = (LogoutResponse) ((SAMLObjectBuilder) SAMLManager.getSAMLBuilder().getBuilder(LogoutResponse.DEFAULT_ELEMENT_NAME)).buildObject();
			response.setID(IDGenerator.randomID());
			response.setVersion(SAMLVersion.VERSION_20);

			if (inResponseTo != null) {
				response.setInResponseTo(inResponseTo);
			}

			SAMLObjectBuilder<Issuer> issuerBuilder = (SAMLObjectBuilder<Issuer>) builderFactory.getBuilder(Issuer.DEFAULT_ELEMENT_NAME);
			Issuer issuer = issuerBuilder.buildObject();
			issuer.setValue(issuerURL);
			response.setIssuer(issuer);

			DateTime now = new DateTime(DateTimeZone.UTC);
			response.setIssueInstant(now);

			SAMLObjectBuilder statusCodeBuilder = (SAMLObjectBuilder) SAMLManager.getSAMLBuilder().getBuilder(StatusCode.DEFAULT_ELEMENT_NAME);
			StatusCode statusCodeElement = (StatusCode) statusCodeBuilder.buildObject();
			statusCodeElement.setValue(statusCode);

			SAMLObjectBuilder statusBuilder = (SAMLObjectBuilder) SAMLManager.getSAMLBuilder().getBuilder(Status.DEFAULT_ELEMENT_NAME);
			Status status = (Status) statusBuilder.buildObject();
			status.setStatusCode(statusCodeElement);

			response.setStatus(status);
			response.setDestination(destinationURL);

		}
		catch (Exception e) {
			logger.error(e.getMessage(), e);
		}
		finally {
		}

		return response;
	}

	/**
	 * 
	 * @param message
	 * @param property
	 * @return
	 * @throws SAMLException
	 */
	private String getAuthProperty(String message, String property) throws SAMLException {
		String result = "";
		Document doc = SAMLUtil.createJdomDoc(message);
		if (doc != null) {
			result = doc.getRootElement().getAttributeValue(property);
		}
		else {
			throw new SAMLException("Error parsing AuthnRequest XML: Null document");
		}

		return result;
	}

	/**
	 * 
	 * @param samlObject
	 * @return
	 * @throws MarshallingException
	 */
	public static String generateSAMLXml(XMLObject samlObject) throws MarshallingException {
		Marshaller marshaller = Configuration.getMarshallerFactory().getMarshaller(samlObject);
		Element authDOM = marshaller.marshall(samlObject);
		StringWriter rspWrt = new StringWriter();
		XMLHelper.writeNode(authDOM, rspWrt);

		return rspWrt.toString();
	}

	/**
	 * 
	 * @param samlXml
	 * @return
	 */
	public static XMLObject getSamlObject(String samlXml) {
		XMLObject result = null;

		try {
			SAMLManager.getSAMLBuilder();
			InputStream inputStream = new ByteArrayInputStream(samlXml.getBytes("UTF-8"));
			ParserPool parserPoolManager = Configuration.getParserPool();
			org.w3c.dom.Document document;
			document = parserPoolManager.parse(inputStream);
			Element metadataRoot = document.getDocumentElement();

			QName qName = new QName(metadataRoot.getNamespaceURI(), metadataRoot.getLocalName(), metadataRoot.getPrefix());

			// get an unmarshaller
			Unmarshaller unmarshaller = Configuration.getUnmarshallerFactory().getUnmarshaller(qName);

			// unmarshall using the document root element
			result = unmarshaller.unmarshall(metadataRoot);

		}
		catch (XMLParserException e) {
			// TODO: Exception management
			logger.error(e.getLocalizedMessage(), e);
		}
		catch (UnmarshallingException e) {
			// TODO: Exception management
			logger.error(e.getLocalizedMessage(), e);
		}
		catch (ConfigurationException e) {
			// TODO: Exception management
			logger.error(e.getLocalizedMessage(), e);
		}
		catch (UnsupportedEncodingException e) {
			// TODO Auto-generated catch block
			logger.error(e.getLocalizedMessage(), e);
		}
		finally {
		}

		return result;

	}

	/**
	 * 
	 * @return
	 * @throws ConfigurationException
	 */
	public Signature getResponseSign() throws ConfigurationException {

		SignatureBuilder signBuilder = (SignatureBuilder) SAMLManager.getSAMLBuilder().getBuilder(Signature.DEFAULT_ELEMENT_NAME);
		KeyInfoBuilder keyInfoBuilder = (KeyInfoBuilder) SAMLManager.getSAMLBuilder().getBuilder(KeyInfo.DEFAULT_ELEMENT_NAME);

		X509Credential credential = new BasicX509Credential();

		KeyInfo keyInfo = (KeyInfo) keyInfoBuilder.buildObject();

		Signature signature = (Signature) signBuilder.buildObject();
		signature.setSignatureAlgorithm(SignatureConstants.ALGO_ID_SIGNATURE_RSA_SHA1);
		signature.setCanonicalizationAlgorithm(SignatureConstants.ALGO_ID_C14N_EXCL_OMIT_COMMENTS);

		signature.setNil(false);

		signature.setSigningCredential(credential);
		signature.setKeyInfo(keyInfo);

		return signature;
	}

	/**
	 * 
	 * @param signature
	 * @return
	 * @throws ConfigurationException
	 */
	public boolean validateRequestSignature(Signature signature) throws ConfigurationException {
		boolean result = false;

		try {
			// result =
			// facade.getServiceSignProvider().validate(StringUtils.EMPTY,
			// signature.getSigningCredential().getEntityId().getBytes());
		}
		catch (Exception e) {
			logger.error(e.getLocalizedMessage());
		}
		finally {
		}

		return result;
	}
}