/**
 * 
 */
package ro.setmobile.idprovider.saml.utilities;

import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.security.GeneralSecurityException;
import java.security.Key;
import java.security.KeyStore;
import java.security.KeyStoreException;
import java.util.Collections;
import java.util.List;

import org.apache.log4j.Logger;

/**
 * @author dan
 * 
 */
public class KeyStoreUtil {

	private static Logger		logger	= Logger.getLogger(KeyStoreUtil.class.getName());

	private static final String	endLine	= System.getProperty("line.separator");

	/**
	 * 
	 * @param filename
	 * @param password
	 * @return
	 * @throws KeyStoreException
	 */
	public static KeyStore getKeyStore(String filename, String password) throws KeyStoreException {

		KeyStore result = KeyStore.getInstance(KeyStore.getDefaultType());

		try {
			FileInputStream in = new FileInputStream(filename);
			result.load(in, password.toCharArray());
			in.close();
		}
		catch (Exception ex) {
			logger.error(ex.getLocalizedMessage());
		}

		return result;
	}

	/**
	 * 
	 * @param in
	 * @param password
	 * @return
	 * @throws KeyStoreException
	 */
	public static KeyStore getKeyStore(InputStream in, String password) throws KeyStoreException {

		KeyStore result = KeyStore.getInstance(KeyStore.getDefaultType());

		try {
			result.load(in, password.toCharArray());
		}
		catch (Exception ex) {
			logger.error(ex.getLocalizedMessage());
		}

		return result;
	}

	/**
	 * 
	 * @param keystore
	 * @return
	 * @throws KeyStoreException
	 */
	@SuppressWarnings("rawtypes")
	public static List getAliases(KeyStore keystore) throws KeyStoreException {

		return Collections.list(keystore.aliases());
	}

	/**
	 * 
	 * @param keystore
	 * @param alias
	 * @param password
	 * @return
	 * @throws GeneralSecurityException
	 */
	public static Key getKey(KeyStore keystore, String alias, String password) throws GeneralSecurityException {

		return keystore.getKey(alias, password.toCharArray());
	}

	/**
	 * Get a certificate from the keystore by name.
	 */
	public static java.security.cert.Certificate getCertificate(KeyStore keystore, String alias) throws GeneralSecurityException {

		return keystore.getCertificate(alias);
	}

	/**
	 * 
	 * @param key
	 * @return
	 */
	public static String spillBeans(Key key) {

		StringBuffer buffer = new StringBuffer("Algorithm: " + key.getAlgorithm() + endLine + "Key value: " + endLine);

		appendHexValue(buffer, key.getEncoded());

		return buffer.toString();
	}

	/**
	 * 
	 * @param cert
	 * @return
	 * @throws GeneralSecurityException
	 */
	public static String spillBeans(java.security.cert.Certificate cert) throws GeneralSecurityException {

		StringBuffer buffer = new StringBuffer("Certificate type: " + cert.getType() + endLine + "Encoded data: " + endLine);
		appendHexValue(buffer, cert.getEncoded());

		return buffer.toString();
	}

	/**
	 * 
	 * @param prompt
	 * @return
	 * @throws IOException
	 */
	public static String getUserInput(String prompt) throws IOException {

		BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
		String result = reader.readLine();

		return result;
	}

	/**
	 * 
	 * @param buffer
	 * @param b
	 */
	public static void appendHexValue(StringBuffer buffer, byte b) {

		int[] digits = { (b >>> 4) & 0x0F, b & 0x0F };
		for (int d = 0; d < digits.length; ++d) {
			int increment = ((digits[d] < 10) ? '0' : ('a' - 10));
			buffer.append((char) (digits[d] + increment));
		}
	}

	/**
	 * 
	 * @param buffer
	 * @param bytes
	 */
	public static void appendHexValue(StringBuffer buffer, byte[] bytes) {

		for (int i = 0; i < bytes.length; ++i) {
			appendHexValue(buffer, bytes[i]);
		}
	}

}
