/**
 * 
 */
package ro.setmobile.idprovider.saml.utilities;

import java.util.UUID;

/**
 * @author dan
 * 
 */
public class IDGenerator {

	/**
	 * 
	 * @return
	 */
	public static String randomID() {

		return UUID.randomUUID().toString();
	}

	/**
	 * 
	 * @return
	 */
	public static  UUID randomUUID() {

		return UUID.randomUUID();
	}
}
