/**
 * 
 */
package ro.setmobile.idprovider.saml.utilities;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.PrintWriter;
import java.io.Writer;

import javax.xml.parsers.SAXParserFactory;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;

import org.apache.log4j.Logger;
import org.w3c.dom.Document;
import org.xml.sax.Attributes;
import org.xml.sax.SAXException;
import org.xml.sax.helpers.DefaultHandler;

/**
 * @author dan
 * 
 */
public class PrettyPrinter extends DefaultHandler {

	private static Logger		logger			= Logger.getLogger(PrettyPrinter.class.getName());

	private String				indent			= "";
	private StringBuffer		currentValue	= null;
	private StringBuffer		output			= new StringBuffer();

	private boolean				justHitStartTag;

	private static final String	standardIndent	= "  ";
	private static final String	endLine			= System.getProperty("line.separator");

	/**
	 * 
	 * @param content
	 * @return
	 */
	public static String prettyPrint(byte[] content) {

		try {
			PrettyPrinter pretty = new PrettyPrinter();
			SAXParserFactory factory = SAXParserFactory.newInstance();
			factory.setFeature("http://xml.org/sax/features/namespace-prefixes", true);
			factory.newSAXParser().parse(new ByteArrayInputStream(content), pretty);
			return pretty.toString();
		}
		catch (Exception ex) {
			logger.error(ex.getLocalizedMessage());
			return "EXCEPTION: " + ex.getClass().getName() + " saying \"" + ex.getMessage() + "\"";
		}
	}

	/**
	 * 
	 * @param content
	 * @return
	 */
	public static String prettyPrint(String content) {

		try {
			PrettyPrinter pretty = new PrettyPrinter();
			SAXParserFactory factory = SAXParserFactory.newInstance();
			factory.setFeature("http://xml.org/sax/features/namespace-prefixes", true);
			factory.newSAXParser().parse(content, pretty);
			return pretty.toString();
		}
		catch (Exception ex) {
			logger.error(ex.getLocalizedMessage());
			return "EXCEPTION: " + ex.getClass().getName() + " saying \"" + ex.getMessage() + "\"";
		}
	}

	/**
	 * 
	 * @param content
	 * @return
	 */
	public static String prettyPrint(InputStream content) {

		try {
			PrettyPrinter pretty = new PrettyPrinter();
			SAXParserFactory factory = SAXParserFactory.newInstance();
			factory.setFeature("http://xml.org/sax/features/namespace-prefixes", true);
			factory.newSAXParser().parse(content, pretty);
			return pretty.toString();
		}
		catch (Exception ex) {
			logger.error(ex.getLocalizedMessage());
			return "EXCEPTION: " + ex.getClass().getName() + " saying \"" + ex.getMessage() + "\"";
		}
	}

	/**
	 * 
	 * @param doc
	 * @return
	 * @throws TransformerException
	 */
	public static String prettyPrint(Document doc) throws TransformerException {

		try {
			ByteArrayOutputStream buffer = new ByteArrayOutputStream();
			TransformerFactory.newInstance().newTransformer().transform(new DOMSource(doc), new StreamResult(buffer));
			byte[] rawResult = buffer.toByteArray();
			buffer.close();

			return prettyPrint(rawResult);
		}
		catch (Exception ex) {
			logger.error(ex.getLocalizedMessage());
			return "EXCEPTION: " + ex.getClass().getName() + " saying \"" + ex.getMessage() + "\"";
		}
	}

	public static class StreamAdapter extends OutputStream {

		public StreamAdapter(Writer finalDestination) {

			this.finalDestination = finalDestination;
		}

		@Override
		public void write(int b) {

			out.write(b);
		}

		public void flushPretty() throws IOException {

			PrintWriter finalPrinter = new PrintWriter(finalDestination);
			finalPrinter.println(PrettyPrinter.prettyPrint(out.toByteArray()));
			finalPrinter.close();
			out.close();
		}

		private ByteArrayOutputStream	out	= new ByteArrayOutputStream();
		Writer							finalDestination;
	}

	/**
	 * 
	 */
	@Override
	public String toString() {

		return output.toString();
	}

	/**
	 * 
	 */
	@Override
	public void startDocument() throws SAXException {

		output.append("<?xml version=\"1.0\" encoding=\"UTF-8\" ?>").append(endLine);
	}

	/**
	 * 
	 */
	@Override
	public void endDocument() throws SAXException {

		output.append(endLine);
	}

	/**
	 * 
	 */
	@Override
	public void startElement(String URI, String name, String qName, Attributes attributes) throws SAXException {

		if (justHitStartTag) {
			output.append('>');
		}

		output.append(endLine).append(indent).append('<').append(qName);

		int length = attributes.getLength();
		for (int a = 0; a < length; ++a) {
			output.append(endLine).append(indent).append(standardIndent).append(attributes.getQName(a)).append("=\"").append(attributes.getValue(a)).append('\"');
		}

		if (length > 0) {
			output.append(endLine).append(indent);
		}

		indent += standardIndent;
		currentValue = new StringBuffer();
		justHitStartTag = true;
	}

	/**
	 * 
	 */
	@Override
	public void endElement(String URI, String name, String qName) throws SAXException {

		indent = indent.substring(0, indent.length() - standardIndent.length());

		if (currentValue == null) {
			output.append(endLine).append(indent).append("</").append(qName).append('>');
		}
		else if (currentValue.length() != 0) {
			output.append('>').append(currentValue.toString()).append("</").append(qName).append('>');
		}
		else {
			output.append("/>");
		}

		currentValue = null;
		justHitStartTag = false;
	}

	/**
	 * 
	 */
	@Override
	public void characters(char[] chars, int start, int length) throws SAXException {

		if (currentValue != null) {
			currentValue.append(escape(chars, start, length));
		}
	}

	/**
	 * 
	 * @param chars
	 * @param start
	 * @param length
	 * @return
	 */
	private static String escape(char[] chars, int start, int length) {

		StringBuffer result = new StringBuffer();
		for (int c = start; c < (start + length); ++c) {
			if (chars[c] == '<') {
				result.append("&lt;");
			}
			else if (chars[c] == '&') {
				result.append("&amp;");
			}
			else {
				result.append(chars[c]);
			}
		}

		return result.toString();
	}

}
