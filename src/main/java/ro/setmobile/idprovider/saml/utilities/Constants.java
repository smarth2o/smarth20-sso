/**
 * 
 */
package ro.setmobile.idprovider.saml.utilities;

/**
 * @author dan
 * 
 */
public class Constants {

	/**
	 * 
	 */
	public final static String	ISSUER				= "http://metaboli.fr";

	/**
	 * 
	 */
	public final static String	NICKNAME			= "Nickname";

	/**
	 * 
	 */
	public final static String	EXPUID			= "expuid";

	/**
	 * 
	 */
	public final static String	NAME_ID				= "nameId";

	/**
	 * 
	 */
	public final static String	SERVICE_URL_NAME	= "AssertionConsumerServiceURL";

	/**
	 * 
	 */
	public final static String	DESTINATION			= "destination";

	/**
	 * 
	 */
	public final static String	USER_NAME			= "userName";

	/**
	 * 
	 */
	public final static String	WEB_SITE			= "Metaboli";

	/**
	 * 
	 */
	public final static String	CONTEXTCLASS_REF	= "urn:oasis:names:tc:SAML:2.0:ac:classes:Password";

	/**
	 * 
	 */
	public final static int		SESSION_EXPIRE		= 30;
}
