/**
 * 
 */
package ro.setmobile.idprovider.saml.utilities;

/**
 * @author dan
 * 
 */
@SuppressWarnings("serial")
public class SAMLException extends Exception {

	protected String	message	= "";

	public SAMLException() {

	}

	public SAMLException(Throwable e) {

		super(e);
	}

	public SAMLException(String message) {

		this.message = message;
	}

	public String getMessage() {

		return this.message;
	}

	public String toString() {

		return "SAML exception: " + message;
	}
}
