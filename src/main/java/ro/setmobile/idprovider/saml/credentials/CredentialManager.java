/**
 * 
 */
package ro.setmobile.idprovider.saml.credentials;

import org.opensaml.xml.security.credential.Credential;
import org.springframework.stereotype.Component;

/**
 * @author dan
 * 
 */
@Component("credentialManager")
public class CredentialManager {

	private IDPCredential	idpCredential;

	/**
	 * 
	 */
	public CredentialManager() {
	}

	/**
	 * 
	 * @return
	 */
	public Credential getCredential() {
		return idpCredential;
	}
}
