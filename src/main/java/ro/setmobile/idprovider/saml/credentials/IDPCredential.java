/**
 * 
 */
package ro.setmobile.idprovider.saml.credentials;

import java.security.PrivateKey;
import java.security.PublicKey;
import java.util.Collection;

import javax.crypto.SecretKey;

import org.opensaml.xml.security.credential.Credential;
import org.opensaml.xml.security.credential.CredentialContextSet;
import org.opensaml.xml.security.credential.UsageType;

/**
 * @author dan
 * 
 */
public class IDPCredential implements Credential {

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * org.opensaml.xml.security.credential.Credential#getCredentalContextSet()
	 */
	public CredentialContextSet getCredentalContextSet() {
		// TODO Auto-generated method stub
		return null;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.opensaml.xml.security.credential.Credential#getCredentialType()
	 */
	public Class<? extends Credential> getCredentialType() {
		// TODO Auto-generated method stub
		return null;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.opensaml.xml.security.credential.Credential#getEntityId()
	 */
	public String getEntityId() {
		// TODO Auto-generated method stub
		return null;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.opensaml.xml.security.credential.Credential#getKeyNames()
	 */
	public Collection<String> getKeyNames() {
		// TODO Auto-generated method stub
		return null;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.opensaml.xml.security.credential.Credential#getPrivateKey()
	 */
	public PrivateKey getPrivateKey() {
		// TODO Auto-generated method stub
		return null;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.opensaml.xml.security.credential.Credential#getPublicKey()
	 */
	public PublicKey getPublicKey() {
		// TODO Auto-generated method stub
		return null;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.opensaml.xml.security.credential.Credential#getSecretKey()
	 */
	public SecretKey getSecretKey() {
		// TODO Auto-generated method stub
		return null;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.opensaml.xml.security.credential.Credential#getUsageType()
	 */
	public UsageType getUsageType() {
		// TODO Auto-generated method stub
		return null;
	}
}
