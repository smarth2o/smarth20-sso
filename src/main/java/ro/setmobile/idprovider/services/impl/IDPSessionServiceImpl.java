/**
 * 
 */
package ro.setmobile.idprovider.services.impl;

import java.util.UUID;

import org.springframework.stereotype.Service;

import ro.setmobile.idprovider.saml.beans.AuthUser;
import ro.setmobile.idprovider.services.IDPSessionService;

/**
 * @author dan
 * 
 */
@Service
public class IDPSessionServiceImpl implements IDPSessionService {

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * ro.setmobile.idprovider.services.IDPSessionService#hasActiveSession(java
	 * .lang.String)
	 */
	public boolean hasActiveSession(String nameId) {
		// TODO Auto-generated method stub
		return false;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * ro.setmobile.idprovider.services.IDPSessionService#hasActiveSession(java
	 * .util.UUID)
	 */
	public boolean hasActiveSession(UUID nameId) {
		// TODO Auto-generated method stub
		return false;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * ro.setmobile.idprovider.services.IDPSessionService#addSession(ro.setmobile
	 * .idprovider.saml.beans.AuthUser, java.lang.String)
	 */
	public void addSession(AuthUser user, String nameID) {
		// TODO Auto-generated method stub

	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * ro.setmobile.idprovider.services.IDPSessionService#inactivateSession(
	 * java.lang.String)
	 */
	public void inactivateSession(String nameId) {
		// TODO Auto-generated method stub

	}

}
