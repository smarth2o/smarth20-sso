/**
 * 
 */
package ro.setmobile.idprovider.services.impl;

import java.util.List;

import org.springframework.stereotype.Service;

import ro.setmobile.idprovider.saml.beans.AuthService;
import ro.setmobile.idprovider.saml.beans.AuthUser;
import ro.setmobile.idprovider.saml.beans.Authority;
import ro.setmobile.idprovider.services.IDPService;

/**
 * @author dan
 * 
 */
@Service
public class IDPServiceImpl implements IDPService {

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * ro.setmobile.idprovider.services.IDPService#hasRightsOnService(ro.setmobile
	 * .idprovider.saml.beans.AuthService,
	 * ro.setmobile.idprovider.saml.beans.AuthUser)
	 */
	public boolean hasRightsOnService(AuthService service, AuthUser user) {
		// TODO Auto-generated method stub
		return false;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * ro.setmobile.idprovider.services.IDPService#getAuthoritiesForUser(ro.
	 * setmobile.idprovider.saml.beans.AuthUser)
	 */
	public List<Authority> getAuthoritiesForUser(AuthUser user) {
		// TODO Auto-generated method stub
		return null;
	}

}
