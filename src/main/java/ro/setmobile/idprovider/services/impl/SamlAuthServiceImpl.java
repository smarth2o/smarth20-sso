package ro.setmobile.idprovider.services.impl;

import java.util.UUID;
import java.util.logging.Logger;

import javax.annotation.Resource;

import org.apache.commons.lang.StringUtils;
import org.omg.CORBA.UnknownUserException;
import org.opensaml.saml2.core.AuthnRequest;
import org.opensaml.saml2.core.LogoutRequest;
import org.opensaml.saml2.core.LogoutResponse;
import org.opensaml.saml2.core.Response;
import org.opensaml.saml2.core.StatusCode;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import ro.setmobile.idprovider.saml.beans.AuthService;
import ro.setmobile.idprovider.saml.beans.AuthUser;
import ro.setmobile.idprovider.saml.manager.SAMLManager;
import ro.setmobile.idprovider.services.AuthenticationService;
import ro.setmobile.idprovider.services.IDPService;
import ro.setmobile.idprovider.services.IDPSessionService;
import ro.setmobile.idprovider.services.SamlAuthService;

/**
 * @author dan
 * 
 */
@Service("samlAuthServiceImpl")
public class SamlAuthServiceImpl implements SamlAuthService {

	protected static Logger			logger	= Logger.getLogger(SamlAuthService.class.getName());

	@Autowired
	private AuthenticationService	authenticationService;
	@Autowired
	private IDPSessionService		idpSessionService;
	@Autowired
	private IDPService				idpService;
	@Resource(name = "samlManager")
	private SAMLManager				samlManager;

	public SamlAuthServiceImpl() {

	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * ro.setmobile.idprovider.services.SamlAuthService#doLogIn(org.opensaml
	 * .saml2.core.AuthnRequest)
	 */
	@Transactional
	public String doLogIn(AuthnRequest authnRequest) throws Exception {

		return doLogIn(authnRequest, null);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * ro.setmobile.idprovider.services.SamlAuthService#hasActiveSession(org
	 * .opensaml.saml2.core.AuthnRequest)
	 */
	@Transactional(readOnly = true)
	public boolean hasActiveSession(AuthnRequest authnRequest) {
		boolean result = false;
		if (authnRequest.getSubject() != null && authnRequest.getSubject().getNameID() != null) {
			String nameID = authnRequest.getSubject().getNameID().getValue();
			result = idpSessionService.hasActiveSession(UUID.fromString(nameID));
		}
		return result;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * ro.setmobile.idprovider.services.SamlAuthService#getServiceURL(org.opensaml
	 * .saml2.core.AuthnRequest)
	 */
	@Transactional(readOnly = true)
	public String getServiceURL(AuthnRequest authnRequest) {
		String url = null;
		if (authnRequest != null) {
			url = authnRequest.getAssertionConsumerServiceURL();
		}
		return url;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see ro.setmobile.idprovider.services.SamlAuthService#getPrincipalEmail()
	 */
	public String getPrincipalEmail() {

		String result = "";

		try {
			AuthUser user = (AuthUser) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
			result = user.getEmail();
		}
		catch (Exception e) {
		}

		return result;
	}

	/**
	 * 
	 * @param authnRequest
	 * @param user
	 * @param nameId
	 * @return
	 * @throws UnauthorizedUserException
	 */
	private String doSynchronousLogin(AuthnRequest authnRequest, AuthUser user, String nameId) {

		String result = null;
		if (user != null) {
			String codeUrl = authnRequest.getAssertionConsumerServiceURL();
			if (!StringUtils.isBlank(codeUrl)) {
				return null;
			}
			String nameID = null;

			result = samlManager.doAssertion(user, new AuthService(), nameID);
		}
		return result;
	}

	/**
	 * 
	 */
	private String doSynchronousLogout(LogoutRequest logoutRequest) {

		String result = null;
		if (logoutRequest.getNameID() != null && StringUtils.isNotBlank(logoutRequest.getNameID().getValue())) {
			String nameID = logoutRequest.getNameID().getValue();

			idpSessionService.inactivateSession(nameID);
		}
		return result;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * ro.setmobile.idprovider.services.SamlAuthService#doLogIn(org.opensaml
	 * .saml2.core.AuthnRequest, java.lang.String)
	 */
	public String doLogIn(AuthnRequest authnRequest, String nameId) throws Exception {

		String response = "";
		try {

			AuthUser user = null;
			// String userStr = null;

			try {
				user = (AuthUser) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
			}
			catch (ClassCastException e) {
				// If unknown user, user = anonymous user
				if ("anonymousUser".equals(SecurityContextHolder.getContext().getAuthentication().getName())) {
					// Bad User
					throw new UnknownUserException();
				}
				else {
					throw e;
				}
			}

			Response resp = samlManager.createResponse(this.doSynchronousLogin(authnRequest, user, nameId), authnRequest.getID());
			resp.setSignature(samlManager.getResponseSign());
			response = SAMLManager.generateSAMLXml(resp);
		}
		catch (Exception e) {
			throw e;
		}

		return response;

	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * ro.setmobile.idprovider.services.SamlAuthService#doLogOut(org.opensaml
	 * .saml2.core.LogoutRequest)
	 */
	public String doLogOut(LogoutRequest logoutRequest) throws Exception {

		this.doSynchronousLogout(logoutRequest);
		String response = null;
		try {
			LogoutResponse resp = samlManager.createLogoutResponse(StatusCode.SUCCESS_URI, logoutRequest.getID(), logoutRequest.getDestination(), (logoutRequest.getIssuer() != null && StringUtils.isNotBlank(logoutRequest.getIssuer().getValue())) ? logoutRequest.getIssuer().getValue() : null, logoutRequest.getNameID().getValue());
			response = SAMLManager.generateSAMLXml(resp);
		}
		catch (Exception e) {
			throw e;
		}

		return response;
	}

}
