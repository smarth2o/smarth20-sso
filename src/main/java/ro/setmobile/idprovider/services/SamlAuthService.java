/**
 * 
 */
package ro.setmobile.idprovider.services;

import javax.xml.transform.TransformerException;

import org.opensaml.saml2.core.AuthnRequest;
import org.opensaml.saml2.core.LogoutRequest;
import org.opensaml.xml.io.MarshallingException;

/**
 * @author dan
 * 
 */
public interface SamlAuthService {

	/**
	 * 
	 * @param authnRequest
	 * @return
	 */
	public String doLogIn(AuthnRequest authnRequest, String nameId) throws Exception;

	/**
	 * 
	 * @param authnRequest
	 * @return
	 */
	public String doLogIn(AuthnRequest authnRequest) throws Exception;

	/**
	 * @throws MarshallingException
	 * @throws TransformerException
	 */
	public String doLogOut(LogoutRequest logoutRequest) throws Exception;

	/**
	 * 
	 * @param authnRequest
	 * @return
	 */
	public boolean hasActiveSession(AuthnRequest authRequest);

	/**
	 * 
	 * @param authnRequest
	 * @return
	 */
	public String getServiceURL(AuthnRequest authnRequest);

	/**
	 * 
	 * @return
	 */
	public String getPrincipalEmail();

}
