/**
 * 
 */
package ro.setmobile.idprovider.services;

import java.util.List;

import ro.setmobile.idprovider.saml.beans.AuthService;
import ro.setmobile.idprovider.saml.beans.AuthUser;
import ro.setmobile.idprovider.saml.beans.Authority;

/**
 * @author dan
 * 
 */
public interface IDPService {

	/**
	 * 
	 * @param service
	 * @param user
	 * @return
	 */
	public boolean hasRightsOnService(AuthService service, AuthUser user);

	/**
	 * 
	 * @param user
	 * @return
	 */
	public List<Authority> getAuthoritiesForUser(AuthUser user);
}
