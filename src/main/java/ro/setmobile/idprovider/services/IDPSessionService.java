/**
 * 
 */
package ro.setmobile.idprovider.services;

import java.util.UUID;

import ro.setmobile.idprovider.saml.beans.AuthUser;

/**
 * @author dan
 * 
 */
public interface IDPSessionService {

	/**
	 * 
	 * @param nameId
	 * @return
	 */
	public boolean hasActiveSession(String nameId);

	/**
	 * 
	 * @param nameId
	 * @return
	 */
	public boolean hasActiveSession(UUID nameId);

	/**
	 * 
	 * @param user
	 */
	public void addSession(AuthUser user, String nameID);

	/**
	 * 
	 * @param user
	 */
	public void inactivateSession(String nameId);

}
