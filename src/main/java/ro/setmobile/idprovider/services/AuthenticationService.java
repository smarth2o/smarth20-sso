/**
 * 
 */
package ro.setmobile.idprovider.services;

import org.springframework.dao.DataAccessException;
import org.springframework.security.core.userdetails.UserDetailsService;

import ro.setmobile.idprovider.saml.beans.AuthUser;

/**
 * @author dan
 * 
 */
public interface AuthenticationService extends UserDetailsService {

	/**
	 * 
	 * @param username
	 * @return
	 * @throws DataAccessException
	 */
	public AuthUser loadUserByUsername(String username);

	/**
	 * 
	 * @param userName
	 * @param password
	 * @return
	 */
	public Boolean doRestAuthentication(String userName, String password);

}
