/**
 * 
 */
package ro.setmobile.idprovider.sh20.auth.beans;

/**
 * @author dan
 *
 */
public class Auth {

	private Boolean	auth;

	/**
	 * @return the auth
	 */
	public Boolean getAuth() {
		return auth;
	}

	/**
	 * @param auth
	 *            the auth to set
	 */
	public void setAuth(Boolean auth) {
		this.auth = auth;
	}

}
