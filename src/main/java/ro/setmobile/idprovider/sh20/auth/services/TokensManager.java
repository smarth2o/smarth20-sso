/**
 * 
 */
package ro.setmobile.idprovider.sh20.auth.services;

import org.apache.ws.security.util.Base64;
import org.apache.ws.security.util.UUIDGenerator;
import org.springframework.stereotype.Component;

/**
 * @author dan
 *
 */
@Component
public class TokensManager {

	private final static String	SUFFIX	= "SH2O";
	private final static String	TAG		= " - ";

	/**
	 * 
	 * @return
	 */
	public String generateToken() {
		return UUIDGenerator.getUUID() + TAG + Base64.encode(SUFFIX.getBytes());
	}

	/**
	 * 
	 * @param token
	 * @return
	 */
	public boolean validateToken(String token) {
		return (token.substring(token.lastIndexOf(TAG), token.length()).equals(Base64.encode(SUFFIX.getBytes())));
	}
}
