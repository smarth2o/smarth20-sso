/**
 * 
 */
package ro.setmobile.idprovider.sh20.auth.services;

import org.springframework.stereotype.Component;
import org.springframework.web.client.RestTemplate;

import ro.setmobile.idprovider.sh20.auth.beans.Auth;

/**
 * @author dan
 *
 */
@Component
public class RestAuthenticationManager {

	private RestTemplate	template	= new RestTemplate();
	private static String	AUTH_URL	= "http://89.121.250.90:8083/SmartH2O/ServiceViewSmartH2O/UserValidation/validateUser?user_email=%22%s%22%20&user_password=%22%s%22";

	/**
	 * 
	 * @param userName
	 * @param password
	 * @return
	 */
	public Boolean doAuthentication(String userName, String password) {
		Boolean result = Boolean.FALSE;
		try {
			Auth authResponse = this.template.getForObject(String.format(AUTH_URL, userName, password), Auth.class);
			result = authResponse.getAuth();
		}
		catch (Exception e) {
		}
		return result;
	}
}
