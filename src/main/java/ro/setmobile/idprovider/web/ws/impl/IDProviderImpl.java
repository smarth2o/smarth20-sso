/**
 * 
 */
package ro.setmobile.idprovider.web.ws.impl;

import org.apache.commons.lang3.StringUtils;
import org.apache.log4j.Logger;
import org.opensaml.saml2.core.AuthnRequest;
import org.opensaml.saml2.core.LogoutRequest;
import org.opensaml.saml2.core.impl.LogoutRequestBuilder;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import ro.setmobile.idprovider.services.SamlAuthService;
import ro.setmobile.idprovider.sh20.auth.services.RestAuthenticationManager;
import ro.setmobile.idprovider.sh20.auth.services.TokensManager;
import ro.setmobile.idprovider.web.ws.IDProvider;

/**
 * @author dan
 * 
 */
@Component
public class IDProviderImpl implements IDProvider {

	private static final Logger			logger	= Logger.getLogger(IDProviderImpl.class.getName());

	@Autowired
	private SamlAuthService				samlAuthService;
	@Autowired
	private RestAuthenticationManager	restAuthenticationManager;
	@Autowired
	private TokensManager				tokensManager;

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * ro.setmobile.idprovider.web.ws.IDProvider#logIn(org.opensaml.saml2.core
	 * .AuthnRequest)
	 */
	@Override
	public String logIn(AuthnRequest authnRequest) {

		String result = "";

		try {
			result = samlAuthService.doLogIn(authnRequest);
		}
		catch (Exception e) {
			logger.error(e.getLocalizedMessage());
		}
		return result;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * ro.setmobile.idprovider.web.ws.IDProvider#logOut(org.opensaml.saml2.core
	 * .AuthnRequest)
	 */
	@Override
	public String logOut(AuthnRequest authnRequest) {
		logger.info("logOut");
		String result = "";

		try {
			LogoutRequest logoutRequest = (new LogoutRequestBuilder()).buildObject(authnRequest.getElementQName());
			result = samlAuthService.doLogOut(logoutRequest);
		}
		catch (Exception e) {
			logger.error(e.getLocalizedMessage());
		}

		return result;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * ro.setmobile.idprovider.web.ws.IDProvider#autehnticate(java.lang.String,
	 * java.lang.String)
	 */
	@Override
	public String authenticate(String userName, String password) {
		logger.info("authenticate");
		String result = StringUtils.EMPTY;
		if (restAuthenticationManager.doAuthentication(userName, password)) {
			result = tokensManager.generateToken();
		}
		return result;
	}

}
