/**
 * 
 */
package ro.setmobile.idprovider.web.ws;

import javax.jws.WebParam;
import javax.jws.WebResult;
import javax.jws.WebService;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

import org.opensaml.saml2.core.AuthnRequest;

/**
 * @author dan
 * 
 */
@WebService(serviceName = "IDProvider")
public interface IDProvider {

	/**
	 * 
	 * @param authnRequest
	 * @return
	 */
	@GET
	@Path("/authenticate/saml")
	@Produces(MediaType.APPLICATION_JSON)
	@WebResult(name = "IDPResponse")
	public String logIn(@WebParam(name = "AuthnRequest") AuthnRequest authnRequest);

	/**
	 * 
	 * @param authnRequest
	 * @return
	 */
	@GET
	@Path("/authenticate/saml/logout")
	@Produces(MediaType.APPLICATION_JSON)
	@WebResult(name = "IDPResponse")
	public String logOut(@WebParam(name = "AuthnRequest") AuthnRequest authnRequest);

	/**
	 * 
	 * @param userName
	 * @param password
	 * @return
	 */
	@GET
	@Path("/authenticate/json")
	@Produces(MediaType.APPLICATION_JSON)
	public String authenticate(@WebParam(name = "username") String userName, @WebParam(name = "password") String password);
}
