package ro.setmobile.idprovider.web.filters;

import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.lang.StringUtils;
import org.springframework.security.authentication.AuthenticationServiceException;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.web.authentication.UsernamePasswordAuthenticationFilter;
import org.springframework.web.context.WebApplicationContext;
import org.springframework.web.context.support.WebApplicationContextUtils;

import ro.setmobile.idprovider.services.AuthenticationService;

/**
 * @author dan
 * 
 */
public class IDPFilter extends UsernamePasswordAuthenticationFilter {

	private boolean					postOnly	= true;

	protected AuthenticationService	authenticationService;

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.springframework.security.web.authentication.
	 * AbstractAuthenticationProcessingFilter
	 * #attemptAuthentication(javax.servlet.http.HttpServletRequest,
	 * javax.servlet.http.HttpServletResponse)
	 */
	@Override
	public Authentication attemptAuthentication(HttpServletRequest request, HttpServletResponse response) throws AuthenticationException {
		this.initialize();

		Authentication result = null;

		if (postOnly && !request.getMethod().equals("POST")) {
			throw new AuthenticationServiceException((new StringBuilder()).append("Authentication method not supported: ").append(request.getMethod()).toString());
		}
		String username = obtainUsername(request);
		String password = obtainPassword(request);

		if (username == null) {
			username = StringUtils.EMPTY;
		}
		if (password == null) {
			password = StringUtils.EMPTY;
		}

		username = username.trim();
		UsernamePasswordAuthenticationToken authRequest = new UsernamePasswordAuthenticationToken(username, password);
		this.setDetails(request, authRequest);

		result = this.getAuthenticationManager().authenticate(authRequest);

		return result;
	}

	/**
	 * 
	 */
	private void initialize() {
		ServletContext servletContext = this.getServletContext();
		WebApplicationContext wac = WebApplicationContextUtils.getRequiredWebApplicationContext(servletContext);

		this.authenticationService = (AuthenticationService) wac.getBean(AuthenticationService.class);
	}

}
