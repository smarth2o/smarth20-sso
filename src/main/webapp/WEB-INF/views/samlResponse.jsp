<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@ include file="/WEB-INF/views/errors/shadowBoxError.jsp"%>

<body onload="showShadowBox();document.sr.submit();">
<script type="text/javascript">
var errorText = 'Error';
	
</script>
<form name="sr" method="post" action="${url}">
		<input type="hidden" name="SAMLResponse" value="${response}" />
		<input type="hidden" name="email" value="${email}" />
		<input type="hidden" name="access" value="${access}" />
</form>
</body>
</html>