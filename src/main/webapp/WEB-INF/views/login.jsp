<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="utf-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html>
<head>
<meta http-equiv="X-UA-Compatible" content="edge" />
<script src='<c:url value="/resources/js/prototype.js"></c:url>' type="text/javascript"></script>
<script src='<c:url value="/resources/js/effects.js"></c:url>' type="text/javascript"></script>
<script src='<c:url value="/resources/js/carousel.js"></c:url>' type="text/javascript"></script>
<link rel="icon" type="image/x-icon" href='<c:url value="/resources/img/favicon.ico"/>' />
<link rel="shortcut icon" type="image/x-icon" href='<c:url value="/resources/img/favicon.ico"/>'>
<meta itemprop="image" content='<c:url value="/resources/img/favicon.ico"/>'>
<link rel="stylesheet" type="text/css" href="<c:url value='/resources/css/style.css' />">
<link rel="stylesheet" type="text/css" href="<c:url value='/resources/css/login.css' />">
</head>

<body id="login" onload="changeHashOnLoad();">

	<div id="screen">
		<div id="main">

			<div id="containerLogin">
				<div id="containerLeft">
					<!-- form START -->
					<form method="post" name="login" action="j_spring_security_check" id="loginForm">
						<c:choose>
							<c:when test="${empty error}">
								<label for="email"><strong>Login :</strong></label>
								<input type="text" autocomplete="on" name="j_username" class="textField" style="line-height: 30px;" id="j_username" value="${email}">
								<label for="password"> <strong>Password :</strong></label>
								<input type="password" autocomplete="on" name="j_password" class="textField" style="line-height: 30px;" id="j_password">
							</c:when>
							<c:otherwise>
								<label for="email"><strong>Login :</strong></label>
								<input type="text" autocomplete="on" name="j_username" class="textField" style="background-color: rgb(255, 204, 204); line-height: 30px;" id="j_username">
								<label for="password"><strong>Password :</strong></label>
								<input type="password" autocomplete="on" name="j_password" class="textField" style="background-color: rgb(255, 204, 204); line-height: 30px;" id="j_password">
							</c:otherwise>
						</c:choose>

						<c:if test="${not empty error}">
							<div class="loginError">
								<div id="errorsheader" class="errorsheader">
									<h2 class="err">Error:</h2>
								</div>
								<div class="errors">
									<p id="loginerror" class="label">login:</p>
									<p id="error" class="description">${error}</p>
								</div>
							</div>
						</c:if>
					</form>
					<!-- form END -->
				</div>

			</div>
		</div>

		<div class="clearfooter"></div>
	</div>
	<script type="text/javascript" src='<c:url value="/resources/js/login.js"></c:url>'></script>
</body>
</html>