<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html>
<head>
<title>SmartH2O Authentication Gateway</title>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<script type="text/javascript" src='<c:url value="/resources/js/shadowbox.js"></c:url>'></script>
<link rel="icon" type="image/x-icon" href='<c:url value="/resources/img/favicon.ico"/>' />
<link rel="stylesheet" type="text/css" href="<c:url value='/resources/css/shadowbox.css' />">
<!--[if lte IE 8]>
    <link href="<c:url value='/resources/css/IE.css' />" rel="stylesheet" type="text/css" />
<![endif]-->

<script type="text/javascript">
	Shadowbox.init();

	function getBaseURL() {
		var url = location.href; // entire url 
		var baseURL = url.substring(0, url.indexOf('/', 14));
		return baseURL + "/";
	}

	function showShadowBox() {
		Shadowbox.open({
			content : 'ERROR',
			player : "html",
			height : 100,
			width : 650,
			options : {
				modal : true,
				animate : false,
			}
		});

	};
</script>

</head>